#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <atomic>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <dlfcn.h>
#include <iostream>
#include <map>
#include <mutex>
#include <set>
#include <vector>

namespace {
    class TRealFunctions {
    public:
        TRealFunctions()
            : Malloc((TMallocFunc)dlsym(RTLD_NEXT, "malloc"))
            , Realloc((TReallocFunc)dlsym(RTLD_NEXT, "realloc"))
            , Free((TFreeFunc)dlsym(RTLD_NEXT, "free"))
        {
        }

        typedef void* (*TMallocFunc)(size_t size);
        typedef void* (*TReallocFunc)(void* ptr, size_t size);
        typedef void (*TFreeFunc)(void* ptr);

        const TMallocFunc Malloc;
        const TReallocFunc Realloc;
        const TFreeFunc Free;
    };

    TRealFunctions* RealFunctions() {
        static TRealFunctions fs;
        return &fs;
    }


    class TLeakDetector {
    private:
        std::mutex Mutex;
        size_t BytesAllocated;
        size_t BytesDeallocated;

        std::map<void*, size_t> Allocated;
        std::set<void*> Freed;

        std::vector<void*> FreeAlreadyFreedAttempts;
        std::vector<void*> FreeUnallocatedAttempts;
        std::vector<void*> BadReallocAttempts;
        std::atomic<bool> Die;

    public:
        TLeakDetector()
            : BytesAllocated(0)
            , BytesDeallocated(0)
            , Die(false)
        {
        }

        void* Malloc(size_t size) {
            std::lock_guard<std::mutex> lock(Mutex);

            void* result = RealFunctions()->Malloc(size);
            //std::cerr << "malloc(" << size << ") = " << result << '\n';

            if (result != nullptr) {
                BytesAllocated += size;
                Allocated[result] = size;
                Freed.erase(result);
            }

            return result;
        }

        void* Realloc(void* ptr, size_t size) {
            std::lock_guard<std::mutex> lock(Mutex);

            bool safeToRealloc = true;

            if (ptr != nullptr) {
                if (Allocated.count(ptr) == 0) {
                    BadReallocAttempts.push_back(ptr);
                    safeToRealloc = false;
                }
            }

            void* result = nullptr;

            if (safeToRealloc) {
                result = RealFunctions()->Realloc(ptr, size);
                //std::cerr << "realloc(" << ptr << ", " << size << ") = " << result << '\n';
                if (result != nullptr) {
                    // realloc() succeeded
                    if (ptr != nullptr) {
                        BytesDeallocated += Allocated[ptr];
                        Allocated.erase(ptr);
                    }
                    Allocated[result] = size;
                    BytesAllocated += size;
                    Freed.erase(result);
                }
            }

            return result;
        }

        void Free(void* ptr) {
            std::lock_guard<std::mutex> lock(Mutex);
            //std::cerr << "free(" << ptr << ")" << '\n';

            // check is it safe to call system free()
            const auto it = Allocated.find(ptr);
            if (it != Allocated.end()) {
                RealFunctions()->Free(ptr);

                BytesDeallocated += it->second;
                Allocated.erase(it);
                Freed.insert(ptr);

            } else {
                if (Freed.count(ptr) != 0) {
                    FreeAlreadyFreedAttempts.push_back(ptr);
                } else {
                    FreeUnallocatedAttempts.push_back(ptr);
                }
            }
        }

        void PrintReport() {
            std::lock_guard<std::mutex> lock(Mutex);
            std::cerr << "====== allochook.so report ======" << '\n';
            std::cerr << "  * bytes allocated: " << BytesAllocated << '\n';
            std::cerr << "  * bytes deallocated: " << BytesDeallocated << '\n';
            bool ok = true;
            if (!Allocated.empty()) {
                ok = false;
                std::cerr << "ERROR: memory leak detected - " << Allocated.size() << " blocks left:\n";
                for (const auto& pair : Allocated) {
                    std::cerr << "    address " << pair.first << " (size " << pair.second << ")\n";
                }
            }
            if (!FreeUnallocatedAttempts.empty()) {
                ok = false;
                std::cerr << "ERROR: " << FreeUnallocatedAttempts.size() << " attempts to free() unallocated memory:\n";
                for (void* addr : FreeUnallocatedAttempts) {
                    std::cerr << "    address " << addr << '\n';
                }
            }
            if (!FreeAlreadyFreedAttempts.empty()) {
                ok = false;
                std::cerr << "ERROR: " << FreeAlreadyFreedAttempts.size() << " attempts to free() the same memory twice or more:\n";
                for (void* addr : FreeAlreadyFreedAttempts) {
                    std::cerr << "    address " << addr << '\n';
                }
            }
            if (!BadReallocAttempts.empty()) {
                ok = false;
                std::cerr << "ERROR: " << BadReallocAttempts.size() << " attempts to realloc() invalid buffer:\n";
                for (void* addr : BadReallocAttempts) {
                    std::cerr << "    address " << addr << '\n';
                }
            }
            if (ok) {
                std::cerr << "OK\n";
            }
        }

        ~TLeakDetector() {
            Die = true;
            // This is done because std::map and std::vector destructors
            // were accounted as free() calls
            PrintReport();
        }

        bool IsAlive() const {
            return !Die;
        }
    };

    TLeakDetector* LeakDetector() {
        static TLeakDetector detector; // thread-safe in C++11
        return &detector;
    }

} // namespace

/**
 * Hooked functions
 */

thread_local bool DisableHook = false;

extern "C"
void* malloc(size_t size) {
    void* res = nullptr;
    if (DisableHook || !LeakDetector()->IsAlive()) {
        res = RealFunctions()->Malloc(size);
    } else {
        DisableHook = true;
        res = LeakDetector()->Malloc(size);
        DisableHook = false;
    }
    return res;
}

extern "C"
void* realloc(void* ptr, size_t size) {
    void* res = nullptr;
    if (DisableHook || !LeakDetector()->IsAlive()) {
        res = RealFunctions()->Realloc(ptr, size);
    } else {
        DisableHook = true;
        res = LeakDetector()->Realloc(ptr, size);
        DisableHook = false;
    }
    return res;
}

extern "C"
void free(void* ptr) {
    if (DisableHook || !LeakDetector()->IsAlive()) {
        RealFunctions()->Free(ptr);
    } else {
        DisableHook = true;
        LeakDetector()->Free(ptr);
        DisableHook = false;
    }
}
