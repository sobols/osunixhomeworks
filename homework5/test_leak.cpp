#include <cstdlib>
#include <cstring>

void Foo() {
    void* ptr = malloc(10);
    memset(ptr, 0, 10);
    free(ptr);
}

void Bar() {
    char* ptr = (char*)malloc(10);
    strcpy(ptr, "abacaba");
}

int main() {
    int* p = new int(42);
    double* q = new double[100];

    char* c = new char[123];
    malloc(78);

    Foo();
    Foo();
    Foo();
    Bar();
    Bar();
    Bar();

    delete[] c;

    return 0;
}
