#include <cstdlib>

int main() {
    char* ptr = (char*)malloc(100);
    free(ptr);
    free(ptr);

    ptr = (char*)malloc(1000);
    free(ptr + 1);
    return 0;
}
