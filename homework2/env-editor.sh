#!/usr/bin/env bash

#set -xue -o pipefail

USAGE="\
envedit [VARIABLE] -- colon-separated environment variable editor

where:
    VARIABLE - env var name (default is PATH)
"

echoerr() {
    echo "ERROR: $@" 1>&2;
}

envedit() {
    if [ $# -eq 0 ]; then
        local VARIABLE="PATH"
    elif [ $# -eq 1 ]; then
        if [ "$1" = "-h" -o "$1" = "--help" ]; then
            echo "$USAGE"
            return 0
        fi
        local VARIABLE="$1"
    else
        echoerr "$USAGE"
        return 1
    fi

    local SAFEPATH="${!VARIABLE:-}"
    local FILENAME=$(mktemp)

    echo -e "${SAFEPATH//:/\\n}" > "$FILENAME"
    if [ $? -ne 0 ]; then
        echoerr "tempfile creation failed"
        return 1
    fi

    ${EDITOR:-vi} "$FILENAME"
    if [ $? -ne 0 ]; then
        echo "text editor execution failed"
        rm -f "$FILENAME"
        return 1
    fi

    # checking if all paths are correct
    while read LINE; do
        if [ ! -d "$LINE" ]; then
            echoerr "directory \"$LINE\" could not be found"
            rm -f "$FILENAME"
            return 1
        fi
    done < "$FILENAME"

    export $VARIABLE=$(tr "\\n" ":" < "$FILENAME" | sed 's/:$//')
    rm -f "$FILENAME"
    return 0
}

#envedit
#export -f envedit
