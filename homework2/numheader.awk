BEGIN {
    FS = OFS = "\t";
    if (ARGC == 2 && (ARGV[1] == "-h" || ARGV[1] == "--help")) {
        print "numheader\nSplits the first line of input by tabs and prints tokens line by line together with 1-based numbers."
        exit 1
    }
}
NR==1 {
    for (i = 1; i <= NF; ++i) print i, $i
    exit
}
