== Задание A4 ==

Справку получается вызвать так:
awk -f numheader -- -h 


== Задание B5 ==

Пусть скрипт сохранён как env-editor.sh.

Использование:
. ./env-editor.sh

Затем вызываем экспортированную функцию:
envedit
envedit -h
envedit VAR
