#include "addrinfo.h"

#include "exception.h"

TAddrInfoWrapper::TAddrInfoWrapper(const std::string& host, unsigned short port)
    : Info(nullptr)
{
    const std::string strPort = std::to_string(port);
    const int error = getaddrinfo(host.c_str(), strPort.c_str(), NULL, &Info);
    if (error != 0) {
        Info = nullptr;
        throw TLinAPIException("getaddrinfo() failed", error);
    }
}

TAddrInfoWrapper::~TAddrInfoWrapper() {
    if (Info != nullptr) {
        freeaddrinfo(Info);
    }
}
