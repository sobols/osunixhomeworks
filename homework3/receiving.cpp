#include "receiving.h"

#include <iostream>
#include <cctype>

std::string URLDecode(const std::string& s) {
    std::string dst;
    char a, b;
    const char* src = s.c_str();
    while (*src) {
        if ((*src == '%') && ((a = src[1]) && (b = src[2]))
                && (isxdigit(a) && isxdigit(b))) {
            if (a >= 'a')
                a -= 'a' - 'A';
            if (a >= 'A')
                a -= ('A' - 10);
            else
                a -= '0';
            if (b >= 'a')
                b -= 'a' - 'A';
            if (b >= 'A')
                b -= ('A' - 10);
            else
                b -= '0';
            dst += char(16 * a + b);
            src += 3;
        } else {
            dst += *src++;
        }
    }
    return dst;
}

namespace NReceiving {

    void TBase::Consume(char ch)
    {
        if (InBody) {
            ProcessBodyChar(ch);
            return;
        }

        if (!LastCharWasCR) {
            if (ch == '\r') {
                LastCharWasCR = true;
            } else {
                Line += ch;
            }

        } else {
            if (ch == '\n') {
                LastCharWasCR = false;
                ProcessLine();
                Line.clear();

            } else if (ch == '\r') {
                LastCharWasCR = true;
                Line += '\r'; // for prev. CR

            } else {
                LastCharWasCR = false;
                Line += '\r'; // for prev. CR
                Line += ch;
            }
        }
    }

    void TBase::ProcessLine()
    {
        if (LineNumber == 0) {
            ProcessFirstLine(Line);
            LineNumber++;
        } else {
            if (!Line.empty()) {
                const size_t pos = Line.find(':');
                if (pos == std::string::npos) {
                    throw std::runtime_error("bad HTTP header: no colon");
                }
                if (pos + 1 >= Line.length()) {
                    throw std::runtime_error("bad HTTP header: no space after colon");
                }
                ProcessHeaderLine(Line.substr(0, pos), Line.substr(pos + 2));
                LineNumber++;
            } else {
                InBody = true;
            }
        }
    }

    void UnitTestHttpRequest()
    {
        {
            THttpRequest request;
            TMemoryInput inp;

            inp = TMemoryInput("GET /example/page HTTP/1.");
            request.Read(&inp);
            assert(!request.IsComplete());

            inp = TMemoryInput("1\r");
            request.Read(&inp);
            assert(!request.IsComplete());

            inp = TMemoryInput("\nHeader: blahblah");
            request.Read(&inp);
            assert(!request.IsComplete());

            inp = TMemoryInput("blah\r\nHost: example.com\r\n");
            request.Read(&inp);
            assert(!request.IsComplete());

            inp = TMemoryInput("\r\nHEAD /\r\n\r\n");
            request.Read(&inp);
            assert(request.IsComplete());
            assert(request.GetMethod() == "GET");
            assert(request.GetURI() == "/example/page");
            assert(inp.HasMoreData());

            // the stream is not finished

            THttpRequest nextRequest;
            nextRequest.Read(&inp);
            assert(nextRequest.IsComplete());
            assert(nextRequest.GetMethod() == "HEAD");
            assert(nextRequest.GetURI() == "/");
            assert(!inp.HasMoreData());
        }
        {
            THttpRequest request;
            TMemoryInput inp;

            inp = TMemoryInput("POST /example/page HTTP/1.1\r\n\r\n");
            request.Read(&inp);
            assert(request.IsComplete());
            assert(request.GetMethod() == "POST");
        }
        std::cerr << "Tests passed" << '\n';
    }

} // namespace NReceiving
