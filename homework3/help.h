#pragma once

#include <string>

bool IsHelpOption(const std::string& s) {
    return (s == "-h" || s == "--help" || s == "-help" || s == "-?");
}
