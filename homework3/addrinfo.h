#pragma once

#include <netdb.h>

#include <string>

class TAddrInfoWrapper {
public:
    TAddrInfoWrapper(const std::string& host, unsigned short port);
    ~TAddrInfoWrapper();

    const sockaddr* GetSockAddrPtr() const {
        return Info->ai_addr;
    }
    socklen_t GetSockAddrLen() const {
        return Info->ai_addrlen;
    }

private:
    TAddrInfoWrapper(const TAddrInfoWrapper&) = delete;
    const TAddrInfoWrapper& operator =(const TAddrInfoWrapper&) = delete;

private:
    addrinfo* Info;
};
