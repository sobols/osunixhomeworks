#pragma once

#include "input.h"
#include "output.h"

#include <cassert>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>

std::string URLDecode(const std::string& s);

namespace NReceiving {

    class TBase {
    public:
        TBase()
            : InBody(false)
            , LastCharWasCR(false)
            , LineNumber(0)
        {
        }
        virtual ~TBase() {
        }

        void Read(TInputStream* in) {
            while (!IsComplete() && in->HasMoreData()) {
                Consume(in->ReadChar());
            }
        }

    private:
        void Consume(char ch);
        void ProcessLine();

    private:
        virtual bool IsComplete() const { return false; }
        virtual void ProcessFirstLine(const std::string& line) = 0;
        virtual void ProcessHeaderLine(const std::string& /*name*/, const std::string& /*value*/) {};
        virtual void ProcessBodyChar(char /*ch*/) {};

    protected:
        bool InBody;
    private:
        bool LastCharWasCR;
        size_t LineNumber;
        std::string Line;
    };

    class THttpRequest : public TBase {
    private:
        // already parsed
        std::string Method;
        std::string URI;

    public:
        virtual bool IsComplete() const {
            return InBody;
        }
        const std::string& GetMethod() const {
            return Method;
        }
        const std::string& GetURI() const {
            return URI;
        }
        friend inline std::ostream& operator <<(std::ostream& out, const THttpRequest& req) {
            return out << req.Method << ' ' << req.URI;
        }

    private:
        virtual void ProcessFirstLine(const std::string& line) {
            size_t pos = line.find(' ');
            if (pos == std::string::npos) {
                throw std::runtime_error("no space found");
            }
            Method = line.substr(0, pos);
            URI = line.substr(pos + 1);

            pos = URI.find(' ');
            if (pos != std::string::npos) {
                // cut HTTP/1.1
                URI.resize(pos);
            }
            URI = URLDecode(URI);
        }
    };

    void UnitTestHttpRequest();


    class THttpResponse : public TBase {
    private:
        // already parsed
        int HttpCode;
        std::string HttpStatus;
        size_t ContentLength;
        TOutputStream* DataStream;
        size_t BodyBytesReceived;
    public:
        THttpResponse(TOutputStream* dataStream)
            : HttpCode(0)
            , ContentLength(0)
            , DataStream(dataStream)
            , BodyBytesReceived(0)
        {
        }

        virtual bool IsComplete() const {
            return InBody && (ContentLength != 0 && BodyBytesReceived == ContentLength);
        }

        int GetHttpCode() const {
            return HttpCode;
        }
        const std::string& GetHttpStatus() const {
            return HttpStatus;
        }
        size_t GetContentLength() const {
            return ContentLength;
        }

    private:
        virtual void ProcessFirstLine(const std::string& line) {
            std::cerr << line << '\n';
            size_t pos1 = line.find(' ');
            if (pos1 == std::string::npos) {
                throw std::runtime_error("no first space found");
            }
            size_t pos2 = line.find(' ', pos1 + 1);
            if (pos2 == std::string::npos) {
                throw std::runtime_error("no second space found");
            }

            HttpCode = std::stoi(line.substr(pos1 + 1, pos2 - pos1 - 1));
            HttpStatus = line.substr(pos2 + 1);
        }

        virtual void ProcessHeaderLine(const std::string& name, const std::string& value) {
            std::cerr << "HEADER " << name << " = " << value << '\n';
            if (name == "Content-Length") {
                ContentLength = std::stoull(value);
            }
        }
        virtual void ProcessBodyChar(char ch) {
            BodyBytesReceived++;
            if (DataStream) {
                DataStream->WriteChar(ch);
            }
        }
    };

} // namespace NReceiving
