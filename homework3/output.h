#pragma once

#include <iostream>
#include <string>

class TOutputStream {
public:
    virtual ~TOutputStream() = default;
    virtual void WriteChar(char ch) = 0;
    virtual void Flush() = 0;

    void WriteAll(const std::string& s) {
        for (char ch : s) {
            WriteChar(ch);
        }
        Flush();
    }
};

class TFDOutput : public TOutputStream {
public:
    TFDOutput(int fd)
        : FD(fd)
        , Pos(0)
    {
    }
    ~TFDOutput() {
        Flush();
    }
    virtual void Flush() {
        size_t written = 0;
        while (written < Pos) {
            ssize_t result = write(FD, Buf + written, Pos - written);
            if (result <= 0) {
                throw TLinAPIException("write() failed");
            }
            written += static_cast<size_t>(result);
        }
        Pos = 0;
    }

    virtual void WriteChar(char ch) {
        if (Pos == BufferSize) {
            Flush();
        }
        Buf[Pos++] = ch;
    }

private:
    enum {
        BufferSize = 4096
    };
    int FD;
    char Buf[BufferSize];
    size_t Pos;
};
