#pragma once

#include "exception.h"
#include "input.h"
#include "output.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <iostream>
#include <cassert>
#include <memory>

void SendAll(int sockfd, const char* data, size_t length);

namespace NSending {
    class TBase {
    public:
        virtual ~TBase() = default;
        virtual void Write(TOutputStream* out) = 0;
    };

    class THttpRequest : public TBase {
    public:
        THttpRequest(const std::string host, const std::string& path)
            : Host(host)
            , Path(path)
        {
        }
        virtual void Write(TOutputStream* out);
    private:
        std::string Host;
        std::string Path;
    };


    class THttpResponse : public TBase {
    public:
        enum class ECode {
            OK,
            NotFound,
            BadRequest,
        };
        THttpResponse(ECode code);
        THttpResponse(const std::string& fileName);
        ~THttpResponse();

        virtual void Write(TOutputStream* out);

        friend inline std::ostream& operator <<(std::ostream& out, const THttpResponse& resp) {
            return out << resp.HeaderText;
        }
    private:
        void DoInit(ECode code, size_t length = 0);
        void WriteCustomBody(int code, const char* msg);
    private:
        std::string HeaderText;
        std::string BodyText;
        int FD;
    };
} // namespace NSending
