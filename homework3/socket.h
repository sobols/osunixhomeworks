#pragma once

#include <unistd.h>

#include <cstring>
#include <utility>

const int INVALID_SOCKET = -1;

class TAddrInfoWrapper;

class TSocketWrapper {
public:
    TSocketWrapper()
        : Socket(INVALID_SOCKET)
    {
    }
    TSocketWrapper(int socket)
        : Socket(socket)
    {
    }
    TSocketWrapper(TSocketWrapper&& other)
        : Socket(INVALID_SOCKET)
    {
        std::swap(Socket, other.Socket);
    }

    int Get() const {
        return Socket;
    }
    int Release() {
        int ret = Socket;
        Socket = INVALID_SOCKET;
        return ret;
    }
    ~TSocketWrapper() {
        if (Socket != INVALID_SOCKET) {
            close(Socket);
        }
    }
    explicit operator bool() const {
        return (Socket != INVALID_SOCKET);
    }
private:
    TSocketWrapper(const TSocketWrapper& other) = delete;
    TSocketWrapper& operator=(const TSocketWrapper&) = delete;

protected:
    int Socket;
};


class TListeningSocketWrapper : public TSocketWrapper {
public:
    TListeningSocketWrapper(unsigned short port);
    TSocketWrapper Accept() const;
};

class TConnectingSocketWrapper : public TSocketWrapper {
public:
    TConnectingSocketWrapper(const TAddrInfoWrapper& addrInfo);
};
