#include "addrinfo.h"
#include "exception.h"
#include "help.h"
#include "input.h"
#include "output.h"
#include "receiving.h"
#include "sending.h"
#include "socket.h"

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

namespace {

    class TParsedURL {
    public:
        std::string Host;
        unsigned short Port;
        std::string Path;

    public:
        TParsedURL()
            : Port(0)
        {}

        TParsedURL(std::string url) {
            const std::string http = "http://";
            if (url.length() < http.length() || url.substr(0, http.length()) != http) {
                throw std::runtime_error("URL must start with http://");
            }

            url = url.substr(http.length());
            size_t pos;

            pos = url.find('/');
            std::string hostPort = (pos == std::string::npos) ? url : url.substr(0, pos);
            Path = url.substr(hostPort.length());

            pos = hostPort.rfind(':');
            if (pos == std::string::npos) {
                Host = hostPort;
                Port = 80;
            } else {
                Host = hostPort.substr(0, pos);
                std::istringstream iss(hostPort.substr(pos + 1));

                if (!(iss >> Port)) {
                    throw std::runtime_error("unable to parse port");
                }
                if (!iss.eof()) {
                    throw std::runtime_error("unable to parse port");
                }
            }

            if (Path.empty()) {
                Path = "/";
            }
        }
    };

    void Main(const std::string& url)
    {
        TParsedURL parsedURL(url);
        std::cerr << "Parsed URL:"
            << " host = " << parsedURL.Host
            << ", port = " << parsedURL.Port
            << ", path = " << parsedURL.Path << std::endl;

        TAddrInfoWrapper addrInfo(parsedURL.Host, parsedURL.Port);
        TConnectingSocketWrapper sock(addrInfo);

        NSending::THttpRequest request(parsedURL.Host, parsedURL.Path);
        TFDOutput socketOutput(sock.Get());
        request.Write(&socketOutput);

        TFDInput socketInput(sock.Get());
        TFDOutput stdOutput(STDOUT_FILENO);

        NReceiving::THttpResponse response(&stdOutput);
        response.Read(&socketInput);

        std::cerr << response.GetHttpCode() << ' ' << response.GetHttpStatus() << '\n';
    }

} // namespace

int main(int argc, char* argv[])
{
    try {
        // Main("http://www.bsu.by/imgbsu/var/logo70.png");
        if (argc != 2 || (argc == 2 && IsHelpOption(argv[1]))) {
            std::cerr << "Usage: " << argv[0] << " URL" << std::endl;
            return EXIT_FAILURE;
        }
        Main(argv[1]);

    } catch (const TLinAPIException& ex) {
            std::cerr << "exception: " << ex.what() << std::endl;
            std::cerr << "specific error code = " << ex.GetErrorCode() << ", errno = " << ex.GetErrNo() << std::endl;
            return EXIT_FAILURE;
    } catch (const std::exception& ex) {
        std::cerr << "exception: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    } catch (...) {
        std::cerr << "unknown exception" << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
