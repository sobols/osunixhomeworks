#pragma once

#include "exception.h"

#include <unistd.h>

#include <cstring>

class TInputStream {
public:
    virtual ~TInputStream() = default;
    virtual bool HasMoreData() = 0;
    virtual char ReadChar() = 0;
};

class TMemoryInput : public TInputStream {
public:
    TMemoryInput()
        : Pos(0)
        , End(0)
    {
    }
    TMemoryInput(const char* data, size_t length)
        : Pos(data)
        , End(data + length)
    {
    }
    explicit TMemoryInput(const char* data) // null-terminated
        : Pos(data)
        , End(data + strlen(data))
    {
    }

    virtual bool HasMoreData() {
        return Pos != End;
    }
    virtual char ReadChar() {
        if (!HasMoreData()) {
            throw std::runtime_error("EOF");
        }
        const char ch = *Pos;
        ++Pos;
        return ch;
    }

private:
    const char* Pos;
    const char* End;
};

class TFDInput : public TInputStream {
public:
    TFDInput(int fd)
        : FD(fd)
        , EOFFlag(false)
        , InBuf(0)
        , Pos(0)
    {
    }
    virtual bool HasMoreData() {
        if (Pos < InBuf) {
            return true;
        } else {
            FillBuffer();
            return (Pos < InBuf);
        }
    }
    virtual char ReadChar() {
        if (Pos < InBuf) {
            return Buf[Pos++];
        } else {
            FillBuffer();
            if (Pos < InBuf) {
                throw std::runtime_error("EOF");
            }
            return Buf[Pos++];
        }
    }

private:
    void FillBuffer() {
        if (EOFFlag) {
            return;
        }
        ssize_t result = read(FD, Buf, BufferSize);
        if (result <= 0) {
            EOFFlag = true;
        }
        if (result < 0) {
            throw TLinAPIException("read() failed");
        }
        InBuf = static_cast<size_t>(result);
        Pos = 0;
    }

private:
    enum {
        BufferSize = 4096
    };
    int FD;
    bool EOFFlag;
    char Buf[BufferSize];
    size_t InBuf;
    size_t Pos;
};
