#include "socket.h"

#include "addrinfo.h"
#include "exception.h"

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

TListeningSocketWrapper::TListeningSocketWrapper(unsigned short port)
{
    Socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (Socket == INVALID_SOCKET) {
        throw TLinAPIException("error while opening socket", Socket);
    }

    sockaddr_in serverAddress;
    memset(&serverAddress, 0, sizeof(serverAddress));

    // Fill up the address structure
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(port);

    // Assign local address and port number
    const int bindResult = bind(Socket, (sockaddr *)&serverAddress, sizeof(serverAddress));
    if (bindResult != 0) {
        throw TLinAPIException("error while binding socket", bindResult);
    }

    // Make the socket a listening socket
    const int listenResult = listen(Socket, SOMAXCONN);
    if (listenResult != 0) {
        throw TLinAPIException("error occurred while listening", listenResult);
    }
}

TSocketWrapper TListeningSocketWrapper::Accept() const
{
    return TSocketWrapper(accept(Socket, nullptr, nullptr));
}

TConnectingSocketWrapper::TConnectingSocketWrapper(const TAddrInfoWrapper& addrInfo)
{
    Socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (Socket == INVALID_SOCKET) {
        throw TLinAPIException("error while opening socket", Socket);
    }
    const int error = connect(Socket, addrInfo.GetSockAddrPtr(), addrInfo.GetSockAddrLen());
    if (error != 0) {
        throw TLinAPIException("connect() failed", error);
    }
}
