#include "sending.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sstream>

static const char eoln[] = "\r\n";

const int INVALID_HANDLE = -1;

void SendAll(int sockfd, const char* data, size_t length)
{
    const char* const end = data + length;
    while (data != end) {
        const ssize_t sent = send(sockfd, data, length, 0);
        if (sent == -1) {
            throw TLinAPIException("send() failed");
        }
        //std::cerr << "sent " << sent << " of " << length << '\n';
        assert(sent >= 0);
        data += sent;
    }
}

namespace NSending {

    void THttpRequest::Write(TOutputStream* out)
    {
        std::ostringstream oss;
        oss << "GET " << Path << " HTTP/1.0" << eoln;
        oss << "Host: " << Host << eoln;
        oss << "Connection: close" << eoln;
        oss << eoln;

        out->WriteAll(oss.str());
    }

    THttpResponse::THttpResponse(enum ECode code)
        : FD(INVALID_HANDLE)
    {
        DoInit(code);
    }

    THttpResponse::THttpResponse(const std::string& fileName)
        : FD(INVALID_HANDLE)
    {
        struct stat sb;
        if (stat(fileName.c_str(), &sb) != 0) {
            DoInit(ECode::NotFound);
            return;
        }
        if (!S_ISREG(sb.st_mode)) {
            DoInit(ECode::NotFound);
            return;
        }
        FD = open(fileName.c_str(), O_RDONLY);
        if (FD == -1) {
            DoInit(ECode::NotFound);
            return;
        }
        DoInit(ECode::OK, sb.st_size);
    }

    void THttpResponse::DoInit(ECode code, size_t length)
    {
         std::ostringstream oss;
         switch (code) {
         case ECode::OK:
             oss << "HTTP/1.0 200 OK" << eoln;
             break;

         case ECode::NotFound:
             oss << "HTTP/1.0 404 Not Found" << eoln;
             oss << "Content-Type: text/html" << eoln;
             WriteCustomBody(404, "Not Found");
             break;

         case ECode::BadRequest:
             oss << "HTTP/1.0 400 Bad Request" << eoln;
             oss << "Content-Type: text/html" << eoln;
             WriteCustomBody(400, "Bad Request");
             break;
         }
         oss << "Connection: Close" << eoln;
         oss << "Content-Length: " << (!BodyText.empty() ? BodyText.size() : length) << eoln;
         oss << eoln;

         HeaderText = std::move(oss.str());
    }

    void THttpResponse::Write(TOutputStream* out)
    {
        out->WriteAll(HeaderText);
        if (!BodyText.empty()) {
            out->WriteAll(BodyText);
        } else {
            TFDInput inp(FD);
            while (inp.HasMoreData()) {
                const char ch = inp.ReadChar();
                out->WriteChar(ch);
            }
        }
        out->Flush();
    }

    void THttpResponse::WriteCustomBody(int code, const char* message)
    {
        std::ostringstream oss;
        oss << "<html>";
        oss << "<body>";
        oss << "<h1>" << code << " &mdash; " << message << "</h1>";
        oss << "<hr>";
        oss << "</body>";
        oss << "</html>";
        BodyText = std::move(oss.str());
    }

    THttpResponse::~THttpResponse()
    {
        if (FD != INVALID_HANDLE) {
            close(FD);
        }
    }

} // namespace NSending
