#pragma once

#include <errno.h>

#include <exception>
#include <stdexcept>

class TLinAPIException : public std::runtime_error {
public:
    TLinAPIException(const char* message, int errorCode = 0)
        : std::runtime_error(message)
        , ErrorCode(errorCode)
        , ErrNo(errno)
    {
    }
    int GetErrorCode() const {
        return ErrorCode;
    }
    int GetErrNo() const {
        return ErrNo;
    }

private:
    int ErrorCode;
    int ErrNo;
};
