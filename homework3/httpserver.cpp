#include "exception.h"
#include "help.h"
#include "input.h"
#include "output.h"
#include "receiving.h"
#include "sending.h"
#include "socket.h"

#include <cstdint>
#include <cstdlib>
#include <climits>
#include <iostream>
#include <sstream>
#include <string>
#include <memory>

struct TThreadContext {
    TSocketWrapper Sock;
    std::string Home;

    TThreadContext(TSocketWrapper&& sock)
        : Sock(std::move(sock))
    {
    }
    ~TThreadContext()
    {
    }
};

std::string RealPath(const std::string& p)
{
    char buf[PATH_MAX];
    if (realpath(p.c_str(), buf)) {
        return std::string(buf);
    } else {
        return std::string();
    }
}

bool IsSubstring(const std::string& text, const std::string& subs)
{
    return (subs.length() <= text.length()) && (text.substr(0, subs.length()) == subs);
}

void Serve(const TThreadContext& ctx)
{
    TFDInput sockIn(ctx.Sock.Get());

    NReceiving::THttpRequest request;
    request.Read(&sockIn);

    typedef NSending::THttpResponse TResp;
    std::unique_ptr<TResp> response;

    if (request.GetMethod() == "GET") {
        const std::string home = RealPath(".");
        const std::string requested = RealPath("." + request.GetURI());
        if (!home.empty() && !requested.empty() && IsSubstring(requested, home)) {
            response.reset(new TResp(requested));
        } else {
            response.reset(new TResp(TResp::ECode::NotFound));
        }
    } else {
        response.reset(new TResp(TResp::ECode::BadRequest));
    }

    std::cerr << ">>>>>>>>\nREQUEST: " << request << "\nRESPONSE HEADER:\n" << *response << "<<<<<<<<\n";

    TFDOutput sockOut(ctx.Sock.Get());
    response->Write(&sockOut);
}

void* WorkerThread(void* ptr)
{
    try {
        std::unique_ptr<TThreadContext> ctx(reinterpret_cast<TThreadContext*>(ptr));
        Serve(*ctx);
    } catch (const TLinAPIException& ex) {
        std::cerr << "exception in worker thread: " << ex.what() << std::endl;
        std::cerr << "specific error code = " << ex.GetErrorCode() << ", errno = " << ex.GetErrNo() << std::endl;
    } catch (const std::exception& ex) {
        std::cerr << "std exception in worker thread: " << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "unknown exception in worker thread" << std::endl;
    }
    return nullptr;
}

class TThreadAttrsWrapper {
public:
    TThreadAttrsWrapper() {
        const int error = pthread_attr_init(&Attr);
        if (error) {
            throw new TLinAPIException("pthread_attr_init()", error);
        }
    }
    pthread_attr_t* Get() {
        return &Attr;
    }
    void SetDetached() {
        const int error = pthread_attr_setdetachstate(&Attr, PTHREAD_CREATE_DETACHED);
        if (error) {
            throw new TLinAPIException("pthread_attr_setdetachstate()", error);
        }
    }
    ~TThreadAttrsWrapper() {
        pthread_attr_destroy(&Attr);
    }
private:
    pthread_attr_t Attr;
};

void Main(unsigned short port)
{
    TListeningSocketWrapper listenSock(port);

    TThreadAttrsWrapper threadAttrs;
    while (true) {
        TSocketWrapper socket = listenSock.Accept();
        if (socket) {
            pthread_t tID = 0;

            std::unique_ptr<TThreadContext> ctx(new TThreadContext(std::move(socket)));

            const int err = pthread_create(&tID, threadAttrs.Get(), WorkerThread, ctx.get());
            if (err == 0) { // no error
                ctx.release();
            } else {
                throw TLinAPIException("pthread_create()", err);
            }
        }
    }
}

int main(int argc, char* argv[])
{
    try {
        if (!(argc == 1 || argc == 2) || (argc == 2 && IsHelpOption(argv[1]))) {
            std::cerr << "Usage: " << argv[0] << " [PORT]" << std::endl;
            return EXIT_FAILURE;
        }

        const int defaultPort = 80;
        const int port = (argc == 2) ? std::stoi(argv[1]) : defaultPort;

        Main(static_cast<unsigned short>(port));

    } catch (const TLinAPIException& ex) {
            std::cerr << "exception: " << ex.what() << std::endl;
            std::cerr << "specific error code = " << ex.GetErrorCode() << ", errno = " << ex.GetErrNo() << std::endl;
            return EXIT_FAILURE;
    } catch (const std::exception& ex) {
        std::cerr << "exception: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    } catch (...) {
        std::cerr << "unknown exception" << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
