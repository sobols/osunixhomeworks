#include <linux/init.h>		/* __init and __exit macroses */
#include <linux/kernel.h>	/* KERN_INFO macros */
#include <linux/module.h>	/* required for all kernel modules */

#include <linux/fs.h>		/* struct file_operations, struct file */
#include <linux/miscdevice.h>	/* struct miscdevice and misc_[de]register() */
#include <linux/mutex.h>	/* mutexes */
#include <linux/string.h>	/* strcmp() function */
#include <linux/slab.h>		/* kzalloc() function */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sergei Sobol <sergei_sobol@tut.by>");
MODULE_DESCRIPTION("In-kernel lexicographically smallest strings finder");


/**
 * lexmin_collector
 */
#define MAX_STRING_COUNT 10

#define LENGTH_PAGE_ORDER 0
#define MAX_LENGTH (PAGE_SIZE * (1 << LENGTH_PAGE_ORDER))
// PAGE_SIZE = 4096, MAX_LENGTH = 4 KB * 2 ^ LENGTH_PAGE_ORDER

struct lexmin_collector {
    char* cur_string;
    size_t cur_pos;

    char* strings[MAX_STRING_COUNT];
    size_t count;

    bool read_mode;
    size_t read_index;
    size_t read_pos;
};

static char* lexmin_create_string(void)
{
    return (char*)__get_free_pages(GFP_KERNEL, LENGTH_PAGE_ORDER);
}
static void lexmin_destroy_string(char** s)
{
    if (*s != NULL) {
        free_pages((unsigned long)(*s), LENGTH_PAGE_ORDER);
        *s = NULL;
    }
}

static void lexmin_collector_destructor(struct lexmin_collector* obj)
{
    if (!obj) {
        return;
    }
    lexmin_destroy_string(&obj->cur_string);
    for (size_t i = 0; i < MAX_STRING_COUNT; ++i) {
        lexmin_destroy_string(&obj->strings[i]);
    }
}

static struct lexmin_collector* lexmin_collector_constructor(struct lexmin_collector* self)
{
    if (self == NULL) {
        return NULL;
    }

    for (size_t i = 0; i < MAX_STRING_COUNT; ++i) {
        self->strings[i] = lexmin_create_string();
        if (unlikely(self->strings[i] == NULL)) {
            goto error;
        }
    }

    self->cur_string = lexmin_create_string();
    if (unlikely(self->cur_string == NULL)) {
        goto error;
    }
    return self;

error:
    lexmin_collector_destructor(self);
    return NULL;
}

static void lexmin_collector_push(struct lexmin_collector* self)
{
    size_t target_pos = self->count;
    char* shifted_out;

    for (size_t i = 0; i < self->count; ++i) {
        if (strcmp(self->cur_string, self->strings[i]) < 0) {
            target_pos = i;
            break;
        }
    }

    if (target_pos >= MAX_STRING_COUNT) {
        return;
    }

    shifted_out = self->strings[MAX_STRING_COUNT - 1];

    for (size_t i = MAX_STRING_COUNT - 1; i > target_pos; --i) {
        self->strings[i] = self->strings[i - 1];
    }
    self->strings[target_pos] = self->cur_string;
    self->cur_string = shifted_out;

    if (self->count < MAX_STRING_COUNT) {
        ++self->count;
    }
}

static void lexmin_collector_add(struct lexmin_collector* self, const char* data, size_t length)
{
    if (self->read_mode) {
        return;
    }

    for (const char* it = data; it != data + length; ++it) {
        if (*it == '\n' || *it == '\0') {
            // end of line
            if (self->cur_pos < MAX_LENGTH) {
                // there was no overflow
                self->cur_string[self->cur_pos] = '\0';
                lexmin_collector_push(self);
            }
            self->cur_pos = 0;

        } else {
            if (self->cur_pos < MAX_LENGTH) {
                // the string is not too long do be stored in our "device"
                self->cur_string[self->cur_pos] = *it;
            }
            ++self->cur_pos;
        }
    }
}

// zero means end of file was reached
static size_t lexmin_collector_dump(struct lexmin_collector* self, char* out, size_t length)
{
    size_t bytes_read = 0;
    const char* s = 0;
    char next;

    self->read_mode = true;

    while (self->read_index < self->count) {
        s = self->strings[self->read_index];
        while (s[self->read_pos] != '\0') {
            next = s[self->read_pos];
            if (likely(bytes_read < length)) {
                out[bytes_read++] = next;
                ++self->read_pos;
            } else {
                return bytes_read;
            }
        }

        next = '\n';
        if (likely(bytes_read < length)) {
            out[bytes_read++] = next;
            self->read_pos = 0;
            ++self->read_index;

        } else {
            return bytes_read;
        }
    }
    return bytes_read;
}


/**
 * lexmin_context
 */

struct lexmin_context {
    struct mutex lock;
    struct lexmin_collector collector;
};

static struct lexmin_context* lexmin_context_create(void)
{
    struct lexmin_context* ctx = NULL;

    ctx = kzalloc(sizeof(*ctx), GFP_KERNEL);
    if (unlikely(ctx == NULL)) {
        return NULL;
    }

    if (unlikely(!lexmin_collector_constructor(&ctx->collector))) {
        kfree(ctx);
        return NULL;
    }

    mutex_init(&ctx->lock);
    return ctx;
}

static void lexmin_context_destroy(struct lexmin_context* ctx)
{
    lexmin_collector_destructor(&ctx->collector);
    kfree(ctx);
}

static int lexmin_open(struct inode *inode, struct file *file)
{
    struct lexmin_context* ctx;
    int err = 0;

    ctx = lexmin_context_create();
    if (unlikely(!ctx)) {
        err = -ENOMEM;
        goto out;
    }

    file->private_data = ctx;

out:
    return err;
}

static ssize_t lexmin_read(struct file* file, char __user* out, size_t size, loff_t* off)
{
    struct lexmin_context* const ctx = file->private_data;
    ssize_t result;

    if (mutex_lock_interruptible(&ctx->lock)) {
        result = -ERESTARTSYS;
        goto out;
    }
    result = lexmin_collector_dump(&ctx->collector, out, size);
    mutex_unlock(&ctx->lock);

out:
    return result;
}

static ssize_t lexmin_write(struct file* file, const char __user* in, size_t size, loff_t* off)
{
    struct lexmin_context *ctx = file->private_data;
    ssize_t result;

    if (mutex_lock_interruptible(&ctx->lock)) {
        result = -ERESTARTSYS;
        goto out;
    }
    if (ctx->collector.read_mode) {
        result = -EIO;
    } else {
        lexmin_collector_add(&ctx->collector, in, size);
        result = size;
    }
    mutex_unlock(&ctx->lock);

out:
    return result;
}

static int lexmin_close(struct inode *inode, struct file *file)
{
    struct lexmin_context *ctx = file->private_data;
    lexmin_context_destroy(ctx);
    return 0;
}

static struct file_operations lexmin_fops = {
        .owner = THIS_MODULE,
        .open = lexmin_open,
        .read = lexmin_read,
        .write = lexmin_write,
        .release = lexmin_close,
        .llseek = noop_llseek
};

static struct miscdevice lexmin_misc_device = {
        .minor = MISC_DYNAMIC_MINOR,
        .name = "lexmin",
        .fops = &lexmin_fops
};

static int __init lexmin_init(void) {
    int result;

    result = misc_register(&lexmin_misc_device);

    if (result == 0) {
        printk(KERN_INFO "lexmin device has been registered\n");
    } else {
        printk(KERN_ERR "unable to register a miscellaneous device\n");
    }
    return result;
}

static void __exit lexmin_exit(void) {
    if (misc_deregister(&lexmin_misc_device) == 0) {
        printk(KERN_INFO "lexmin device has been unregistered\n");
    } else {
        printk(KERN_ERR "unable to unregister a miscellaneous device\n");
    }
}

module_init(lexmin_init);
module_exit(lexmin_exit);
