#!/usr/bin/env sh

echo 'SUBSYSTEM=="misc", KERNEL=="lexmin", MODE="0666"' > /lib/udev/rules.d/99-lexmin.rules
insmod ../lexmin.ko
lsmod | grep lexmin

