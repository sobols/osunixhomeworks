#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUF_SIZE 256

void WriteString(int fd, const char* s)
{
    size_t len;
    size_t bytesWritten = 0;
    ssize_t err;

    len = strlen(s);
    while (bytesWritten < len) {
        err = write(fd, s + bytesWritten, len - bytesWritten);
        if (err <= 0) {
            perror("write()");
            exit(EXIT_FAILURE);
        }
        bytesWritten += err;
    }
}

void ReadAll(int fd)
{
    char buf[BUF_SIZE + 1];
    ssize_t err;

    while (1) {
        err = read(fd, buf, BUF_SIZE);
        if (err == 0) {
            printf("%s", "EOF\n");
            break;
        } else if (err < 0) {
            perror("read()");
            exit(EXIT_FAILURE);
        }
        buf[err] = '\0';
        printf("%s", buf);
    }
}

int main()
{
    int fd;

    fd = open("/dev/lexmin", O_RDWR);
    if (fd == -1) {
        perror("open()");
        return EXIT_FAILURE;
    }

    WriteString(fd, "hello\n");
    WriteString(fd, "a\nc\nd\n");
    ReadAll(fd);

    close(fd);
    return EXIT_SUCCESS;
}
